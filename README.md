# IS_TextToSpeech

Get code for Text to Speech by following command

> git clone https://gitlab.com/pavan.saragadam/is_texttospeech.git

Build code

> mvn clean install

> mvn spring-boot:run


To execute API, Please execute following curl url:

curl -X POST \
  http://${HOST}/tts/audio \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: 83bfd2a0-79c9-f93d-9527-e1e89a3744fa' \
  -d '{
	"speachText":"Hello World!"
}

We have to pass text, result will be as wav file.
