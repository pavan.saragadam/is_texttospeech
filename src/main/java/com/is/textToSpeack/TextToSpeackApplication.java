package com.is.textToSpeack;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.is"})
public class TextToSpeackApplication {

	public static void main(String[] args) {
		SpringApplication.run(TextToSpeackApplication.class, args);
	}

}
